﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelPart : MonoBehaviour
{
    

    public Vector3 EnterPoint { get => transform.position + enterPoint; set { } }
    public Vector3 ExitPoint { get => transform.position + exitPoint; set { } }

    public Vector3 EnterPointOffset { get =>  enterPoint; set { } }
    public Vector3 ExitPointOffset { get =>  exitPoint; set { } }

    [SerializeField] Vector3 enterPoint;
    [SerializeField] Vector3 exitPoint;

    [SerializeField] float gizmoRadius = 0.5f;
    //[SerializeField] float coefRunning = 1.5f;
    //[SerializeField] float coefFalling = 3f;
    
    
    GameSettingsInstaller.PlayerSettings _settings;

    

    private void OnDrawGizmos()
    {
        if(_settings == null || _settings.Moving.movingForwardSpeed == 0)
        {
            _settings = ((GameSettingsInstaller) Resources.Load<ScriptableObject>("Settings/GameSettings")).Player;
        }

        Gizmos.color = Color.green;
        Gizmos.DrawSphere(EnterPoint,gizmoRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(ExitPoint, gizmoRadius);
        /*
        var enterPoint = transform.position;
        enterPoint.y += transform.localScale.y / 2f + _settings.Falling.fallingSpeed / coefFalling;
        enterPoint.z -= transform.localScale.z / 2f + _settings.Moving.movingForwardSpeed / coefRunning; 
        Gizmos.DrawSphere(enterPoint,gizmoRadius);

        Gizmos.color = Color.red;

        var exitPoint = transform.position;
        exitPoint.y += transform.localScale.y / 2f - _settings.Falling.fallingSpeed / coefFalling;
        exitPoint.z += transform.localScale.z / 2f + _settings.Moving.movingForwardSpeed / coefRunning;
        Gizmos.DrawSphere(exitPoint, gizmoRadius);
        */

        /*
        var start = transform.position + Vector3.Scale(new Vector3(0, 1, 1), transform.localScale) / 2f + Vector3.up * _settings.Animations.anotherStickPos.y;
        var end = start + (Vector3.down *_settings.Falling.fallingSpeed + Vector3.forward * _settings.Moving.movingForwardSpeed) * 5;
        Gizmos.DrawLine(start,end);
        */
    }
}
