﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LoseManager
{
    private Ragdoll _ragdoll;
    private CameraFollow _camera;
    private IStickable _stick;
    private UiRestartPanel _uiRestartPanel;
    private Move _move;
    private bool _isLost = false;

    [Inject]
    public LoseManager(IStickable stick, Ragdoll ragdoll,CameraFollow camera,UiRestartPanel uiRestartPanel)
    {
        _ragdoll = ragdoll;
        _camera = camera;
        _stick = stick;
        _uiRestartPanel = uiRestartPanel;
        //_move = move;
        _stick.OnStickEnd += Lose;
    }

    public void Lose()
    {
        if (_isLost)
            return;

        _isLost = true;
        _ragdoll.TurnRagdoll();
        _camera.Stop();
        //_move.enabled = false;
        _stick.BecomeFree(_ragdoll.transform);
        _uiRestartPanel.Enable(1.5f);
    }
}
