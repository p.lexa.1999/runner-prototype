﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Saw : MonoBehaviour
{
    [Inject]
    LoseManager _lose;

    private void OnTriggerEnter(Collider other)
    {
        var stick = other.GetComponent<Stick>();

        if (stick != null)
        {
            var contactPoint = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            stick.Cut(contactPoint.x);
        }

        var arc = other.GetComponentInParent<ArcBeh>();
        if (arc != null)
        {
            arc.GetComponentInParent<ArcManager>().Cut(arc);
        }

        var player = other.GetComponent<Move>();
        if (player != null)
        {
            _lose.Lose();
        }
    }

   
}
