﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelManager
{
    private Settings _settings;
    private Installer _installer;
    private SceneController _sceneController;

    [Inject]
    public void Constructor(Settings settings,Installer installer,SceneController sceneController)
    {
        _settings = settings;
        _installer = installer;
        _sceneController = sceneController;
        LoadCurrentLevel();
    }

    public void LoadNextLevel()
    {
        _settings.currentLevel++;
        _sceneController.ReloadScene();
    }

   private void LoadCurrentLevel()
    {
        var res = _installer.LoadLevel(_settings.currentLevel);
        if (!res)
        {
            _settings.currentLevel = 1;
            //LoadCurrentLevel();
            _installer.LoadLevel(_settings.currentLevel);
        }
    }

    [Serializable]
    public class Settings
    {
        public int currentLevel;

    }
}
