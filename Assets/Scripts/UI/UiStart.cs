﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UiStart : MonoBehaviour
{
    [SerializeField]
    private GameObject startPanel;

    private Move _move;
    private AnimationController _animation;

    [Inject]
    public void Constructor(Move move,AnimationController anim)
    {
        _move = move;
        _animation = anim;
    }

    public void StartGame()
    {
        _move.enabled = true;
        _animation.StartRunning();
        startPanel.SetActive(false);
    }

}
