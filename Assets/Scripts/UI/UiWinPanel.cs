﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;

public class UiWinPanel : MonoBehaviour
{
    [SerializeField] GameObject panel;
    [SerializeField] GameObject backPanel;
    [SerializeField] TMP_Text collectedText;

    [Inject]
    private Currency _currency;

    private void Start()
    {
        Disable();
    }

    public void Enable(float time)
    {
        StartCoroutine(EnableAfterTime(time));
        DisplayCollected();
    }

    public void Disable()
    {
        panel.SetActive(false);
        backPanel.SetActive(false);
    }

    private void DisplayCollected()
    {
        var number = _currency.collectedSoftOnLevel;
        collectedText.text = number.ToString();
    }

    private IEnumerator EnableAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        panel.SetActive(true);
        backPanel.SetActive(true);
    }
}
