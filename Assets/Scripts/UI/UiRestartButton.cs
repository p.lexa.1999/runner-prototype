﻿using System.Collections;
using Zenject;
using UnityEngine;

public class UiRestartButton : MonoBehaviour
{
    
    public SceneController _sceneController;

    [Inject]
    public void Constructor(SceneController sceneController)
    {
        _sceneController = sceneController;
        Debug.Log(_sceneController);
    }

    public void Click()
    {
        _sceneController.ReloadScene();
    }
}
