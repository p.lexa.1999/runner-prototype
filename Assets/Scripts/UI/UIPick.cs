﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UIPick : MonoBehaviour
{
    [SerializeField]
    private GameObject digit;

    private ArcManager _arcManager;

    [Inject]
    public void Constructor(ArcManager arcManager)
    {
        _arcManager = arcManager;
        _arcManager.OnExtend += SpawnNewDigit;
    }

    public void SpawnNewDigit(int number)
    {
        Instantiate(digit,transform);
    }
}
