﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiRestartPanel : MonoBehaviour
{
    [SerializeField] GameObject panel;
    [SerializeField] GameObject backPanel;

    private void Start()
    {
        Disable();
    }

    public void Enable(float time)
    {
        StartCoroutine(EnableAfterTime(time));
    }

    public void Disable()
    {
        panel.SetActive(false);
        backPanel.SetActive(false);
    }

    private IEnumerator EnableAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        panel.SetActive(true);
        backPanel.SetActive(true);
    }
}
