﻿using System.Collections;
using Zenject;
using UnityEngine;

public class UiClaimButton : MonoBehaviour
{

    private Currency _currency;
    private LevelManager _levelManager;

    [Inject]
    public void Constructor(Currency currency, LevelManager levelManager)
    {
        _currency = currency;
        _levelManager = levelManager;
        Debug.Log(_currency + " " + _levelManager);
    }

    public void Click()
    {
        //_currency.CompaundSoft();
        //StartCoroutine(LoadNextAfterTime());
        _levelManager.LoadNextLevel();
    }

    private IEnumerator LoadNextAfterTime()
    {
        yield return new WaitForSeconds(2f);
        _levelManager.LoadNextLevel();
    }
}
