﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;

public class UiGamePanel : MonoBehaviour
{
    [SerializeField] TMP_Text levelText;
    [SerializeField] TMP_Text currencyText;


    private Currency _currency;

    [Inject]
    public void Constructor(Currency currency)
    {
        _currency = currency;
        _currency.OnSoftChanged += DisplaySoftCurrency;
        _currency.CallBackRequest();
    }


    public void DisplayLevelNumber(int number)
    {
        levelText.text = number.ToString();
    }

    private void DisplaySoftCurrency(int number)
    {
        currencyText.text = number.ToString();
    }
}
