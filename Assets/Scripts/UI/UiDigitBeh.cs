﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiDigitBeh : MonoBehaviour
{
    public int dist = 100;

    private RectTransform _rect;
    private Vector3 startPos;

    private void Start()
    {
        _rect = GetComponent<RectTransform>();
        startPos = _rect.position;
        StartCoroutine(SmoothMove());
    }

    private IEnumerator SmoothMove()
    {
        var endPos = startPos + Vector3.up * dist;

        for(float i = 0;i<1;i+=0.1f)
        {
            _rect.position = Vector3.Lerp(startPos,endPos,i);
            yield return new WaitForSeconds(0.05f);
        }

        StartCoroutine(SmoothHide());
    }

    private IEnumerator SmoothHide()
    {
        var tmp = GetComponent<TMP_Text>();
        var startCol = tmp.color;
        var endCol = startCol;
        endCol.a = 0;

        for (float i = 0; i < 1; i+=0.1f)
        {
            tmp.color = Color.Lerp(startCol, endCol, i);
            yield return new WaitForSeconds(0.05f);
        }

        Destroy(gameObject);
    }

}
