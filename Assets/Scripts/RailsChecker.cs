﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Collider))]
public class RailsChecker : MonoBehaviour
{
    public bool IsOnRails
    {
        get; private set;
    }

    private string _railTag = "Rail";
    private Fly _fly;
    private List<GameObject> _rails = new List<GameObject>();
    private List<int> _railsTouched = new List<int>();

    [Inject]
    private AnimationController _animation;
    [Inject]
    private LoseManager _loseManager;

    private void Start()
    {
        IsOnRails = false;
        _fly = GetComponentInParent<Fly>();
        if (_fly == null)
            throw new Exception("RailsChecker cant find Fall script in parent");
    }

    private void OnTriggerEnter(Collider other)
    {
        Enter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        Enter(other);
    }

    public void Enter(Collider other)
    {
        if (other.CompareTag(_railTag))
        {
            if (!_rails.Contains(other.gameObject))
            {
                _rails.Add(other.gameObject);
                _railsTouched.Add(0);
            }

            var index = _rails.IndexOf(other.gameObject);
            _railsTouched[index]++;

            Debug.Log(_rails.Count + " " + _railsTouched);

            if (_rails.Count >= 2)
            {
                _fly.DisableFalling();
                _animation.GoRails();
                StopAllCoroutines();
                IsOnRails = true;
            }
            else
            {
                _fly.DisableFalling();
                StartCoroutine(Check2RailsConnected(0.05f));
            }
        }
    }

    public void Exit(Collider other)
    {
        if (other.CompareTag(_railTag))
        {

            var index = _rails.IndexOf(other.gameObject);
            _railsTouched[index]--;

            if (_railsTouched[index] == 0)
            {
                _rails.Remove(other.gameObject);
                _railsTouched.RemoveAt(index);
            }
            

            if (_rails.Count == 1)
            {
                StartCoroutine(Check2RailsConnected(0.1f));
            }
            else if(_rails.Count == 0)
            {
                Debug.Log("Successful exit");
                StopAllCoroutines();
                //StartCoroutine(LeaveRailsAfterTime());
                IsOnRails = false;
                _fly.EnableFalling();
                _animation.LeaveRails();
            }
        }
    }

    private IEnumerator LeaveRailsAfterTime()
    {
        yield return new WaitForSeconds(0);
        _fly.EnableFalling();
        _animation.LeaveRails();
    }

    private IEnumerator Check2RailsConnected(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log("In checker");
        if (_rails.Count == 1)
        {
            Debug.Log("Lose " + _rails.Count + " " + _railsTouched);
            _loseManager.Lose();
        }
              
    } 
}
