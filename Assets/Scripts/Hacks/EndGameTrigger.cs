﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EndGameTrigger : MonoBehaviour
{
    [SerializeField] GameObject player;

    [Inject]
    private LoseManager _lose;

    private void Start()
    {
        player = FindObjectOfType<Move>().gameObject;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            _lose.Lose();
            //player.transform.position = Vector3.zero;
        }
    }

    
}
