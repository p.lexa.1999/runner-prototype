﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour, IStickable
{
    public event Action OnStickEnd;

    [SerializeField] GameObject stickPrefab;

    private Vector3 _initLocalPos;
    private float _currentLenght
    {
        get => transform.localScale.z;
        set
        {
            var scale = transform.localScale;
            transform.localScale = new Vector3(scale.x, scale.y, value);
        }
    }

    private void Start()
    {
        _initLocalPos = transform.localPosition;
    }

    public void Extend(int value)
    {
        _currentLenght = _currentLenght + value;
    }

    public void Shrink(float value)
    {
        if(_currentLenght - value <= 0)
        {
            _currentLenght = 0;
            //Lose
            Debug.Log("Dead");
            return;
        }
        _currentLenght = _currentLenght - value;
        var leftEdge = transform.position.x - _currentLenght / 2f ;
        var rightEdge = transform.position.x + _currentLenght / 2f;
        SpawnPart(value,leftEdge,Vector3.left);
        SpawnPart(value, rightEdge, Vector3.right);
    }

    public void Cut(float xCoord)
    {
        float delta;
        float edge;
        Vector3 dir;

        if(xCoord >= transform.position.x)
        {
            edge = transform.position.x + _currentLenght / 2f;
            dir = Vector3.right;
            delta = edge - xCoord;
        }
        else
        {
            edge = transform.position.x - _currentLenght / 2f;
            dir = Vector3.left;
            delta = -1 * (edge - xCoord);
        }
        _currentLenght -= delta;
        transform.position -= dir * delta / 2f;
        SpawnPart(delta,edge,dir);
        MoveCenter();
    }

    private void MoveCenter()
    {
        SmoothStickMove(_initLocalPos,true);
    }

    public void SmoothStickMove(Vector3 distinationPlace, bool isStartDelay)
    {
        if (transform.parent == null)
            return;

        StopAllCoroutines();
        StartCoroutine(SmoothMove(distinationPlace,isStartDelay));
    }

    private IEnumerator SmoothMove(Vector3 distinationPlace,bool isStartDelay)
    {
        Debug.Log("Call");
        if (isStartDelay)
        {
            Debug.Log("Delay");
            yield return new WaitForSeconds(1f);
        }

        for (float i = 0; i <= 1; i+=0.01f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,distinationPlace,i);
            yield return new WaitForSeconds(0.01f);
        }
    } 

    public void SpawnPart(float size,float edgeX,Vector3 dir)
    {
        var pos = transform.position;
        pos.x = edgeX - dir.x * size / 2f;
        var cutStick = Instantiate(stickPrefab,pos,Quaternion.Euler(0,90,0));
        var scale = cutStick.transform.localScale;
        scale.z = size;
        cutStick.transform.localScale = scale;
    }

    public void BecomeFree(Transform objToParent)
    {
        transform.parent = null;
        gameObject.AddComponent<Rigidbody>();
        GetComponent<CapsuleCollider>().isTrigger = false;
        StopAllCoroutines();
        enabled = false;
    }
}
