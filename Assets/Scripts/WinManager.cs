﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class WinManager
{
    private Ragdoll _ragdoll;
    private CameraFollow _camera;
    private UiWinPanel _uiWinPanel;
    private Move _move;
    private bool _isWin = false;

    [Inject]
    public WinManager(Ragdoll ragdoll,CameraFollow camera,UiWinPanel uiWinPanel)
    {
        _ragdoll = ragdoll;
        _camera = camera;
        _uiWinPanel = uiWinPanel;
       // _move = move;
    }

    public void Win()
    {
        if (_isWin)
            return;

        _isWin = true;
        _ragdoll.TurnRagdoll();
        _camera.Stop();
        //_move.enabled = false;
        _uiWinPanel.Enable(1.5f);
    }
}
