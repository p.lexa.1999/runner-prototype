﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;


public class LavaFloor : MonoBehaviour
{

    private Move _player;
    private bool _isOnFloor = false;
    private IStickable _stick;
    private Settings _settings;

    [Inject]
    public void Constructor(Settings settings, IStickable stick )
    {
        _settings = settings;
        _stick = stick;
    }


    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Move>();
        if (player != null)
        { 
            _player = player;
        
            _isOnFloor = true;
            StartCoroutine(ShrinkStick());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var player = other.GetComponent<Move>();
        if (player != null && _player == player)
        {
            _isOnFloor = false;
        }
    }
    
    private IEnumerator ShrinkStick()
    {
        while (_isOnFloor)
        {
            _stick.Shrink(_settings.shrinkAmount);
            yield return new WaitForSeconds(_settings.periodInSeconds);
        }
    }

    [Serializable]
    public class Settings
    {
        public float periodInSeconds;
        public float shrinkAmount;
    }

}
