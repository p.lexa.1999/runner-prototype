﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Level : MonoBehaviour
{
    [SerializeField] bool buildLevel = false;

    GameSettingsInstaller.PlayerSettings _settings;

    private float _seconds = 0;

    public void DestroyLevel()
    {
        Destroy(gameObject);
    }  

    private void OnValidate()
    {

        if (!buildLevel)
            return;

        if (_settings == null || _settings.Moving.movingForwardSpeed == 0)
        {
            _settings = ((GameSettingsInstaller)Resources.Load<ScriptableObject>("Settings/GameSettings")).Player;
        }

        _seconds = 0;
        var arr = GetComponentsInChildren<LevelPart>();
        

        //parts.OrderBy(t => t.transform.position.z);
        var parts = arr.ToList();
        parts.Sort((p1, p2) => p1.transform.position.z.CompareTo(p2.transform.position.z));

        for (int i = 0; i < parts.Count; i++)
        {
            Debug.Log(parts[i].name);
        }

        float heigh = 0;

        for(int i = 1;i<parts.Count;i++)
        {
            
            heigh = parts[i - 1].ExitPoint.y - parts[i].EnterPoint.y;
            var speed = _settings.Falling.fallingSpeed;
            var accel = _settings.Falling.acceleration;
            
            _seconds = (Mathf.Sqrt(2 * accel * heigh + speed * speed) - speed) / accel;
            var distance = _seconds * _settings.Moving.movingForwardSpeed;
            var pos = parts[i].EnterPoint;
            pos.z = parts[i - 1].ExitPoint.z + distance;
            parts[i].transform.position = pos - parts[i].EnterPointOffset;

            Debug.Log("Name: " + parts[i].name + " heigh delta: " + heigh + " seconds fly: " + _seconds + " distance: " +distance );
        }

        buildLevel = false;
    }
}
