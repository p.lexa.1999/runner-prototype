﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class WinTrigger : MonoBehaviour
{
    //[SerializeField] GameObject player;

    [Inject]
    private LevelManager _levelManager;
    [Inject] GroundChecker player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player.gameObject)
        {
            player.transform.position = Vector3.zero; //spawn new again?
            _levelManager.LoadNextLevel();
            GetComponentInParent<Level>().DestroyLevel();

        }
    }
}
