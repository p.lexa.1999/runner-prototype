﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(Fly)), RequireComponent(typeof(Collider))]
public class GroundChecker : MonoBehaviour
{
    public bool IsOnGround
    {
        get;private set;
    }

    private string _groundTag = "GroundSurface";
    private Fly _fly;

    [Inject]
    private AnimationController _animation;
    [Inject]
    private LoseManager _loseManager;

    private void Start()
    {
        _fly = GetComponent<Fly>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_groundTag))
        {
            var topBound = other.bounds.center + other.bounds.extents;
            if (topBound.y - transform.position.y < 0.1f)
            {
                var pos = transform.position;
                pos.y = topBound.y; 
                transform.position = pos;
                _fly.DisableFalling();
                _animation.Run();
                IsOnGround = true;
            }
            else
            {
                _loseManager.Lose();
                //enabled = false;
                Destroy(this);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(_groundTag))
        {
            _fly.EnableFalling();
            _animation.Fly();
            IsOnGround = false;
        }
    }

   
}
