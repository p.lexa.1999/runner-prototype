﻿using UnityEngine;
using System;
using Zenject;

public class Fly : MonoBehaviour
{ 
    private Settings _settings;
    private bool _isFying = false;
    private AnimationController _animation;
    private float _acceleration = 0;

    [Inject]
    public void Constructor(Settings settings,AnimationController animation)
    {
        _settings = settings;
        _animation = animation;
    }

    public void DisableFalling()
    {
        _isFying = false;
        
    }

    public void EnableFalling()
    {
        _isFying = true;
    }

    private void FixedUpdate()
    {
        if (_isFying)
        {
            _acceleration += _settings.acceleration * Time.deltaTime;
            var speed = Vector3.down * (_acceleration + _settings.fallingSpeed) * Time.deltaTime;
            transform.position += speed;
            //Debug.Log("Speed " + speed);
        }
        else
        {
            _acceleration = 0;
        }
        
    }

    [Serializable]
    public class Settings
    {
        public float fallingSpeed;
        public float acceleration;

    }
}
