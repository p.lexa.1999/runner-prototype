﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class VirtualBorder
{
    private Settings _settings; 

    public VirtualBorder(Settings settings)
    {
        _settings = settings;
    }

    public Vector3 Clamp(Vector3 vector3)
    {
        if (_settings.xBorders.isActive)
        {
            vector3.x = Mathf.Clamp(vector3.x, _settings.xBorders.min, _settings.xBorders.max);
        }
        if (_settings.yBorders.isActive)
        {
            vector3.y = Mathf.Clamp(vector3.y, _settings.yBorders.min, _settings.yBorders.max);
        }
        if (_settings.zBorders.isActive)
        {
            vector3.z = Mathf.Clamp(vector3.z, _settings.zBorders.min, _settings.zBorders.max);
        }
        return vector3;
    }

    [Serializable]
    public class Settings
    {
        public ClampBorders xBorders;
        public ClampBorders yBorders;
        public ClampBorders zBorders;
    }

    [Serializable]
    public class ClampBorders
    {
        public bool isActive;
        public float min;
        public float max;
    }
}
