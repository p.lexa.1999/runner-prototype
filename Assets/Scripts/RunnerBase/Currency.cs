﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

public class Currency
{
    public event Action<int> OnSoftChanged;
   // public event Action<int> OnSoftCompaund;

    public int collectedSoftOnLevel { get; private set; }
    private Settings _settings;


    [Inject]
    public Currency(Settings settings)
    {
        _settings = settings;
    }
    
    public void CollectSoft(int amount)
    {
        collectedSoftOnLevel += amount;
        AddSoft(amount);
    }

    public void CompaundSoft()
    {
        //OnSoftCompaund?.Invoke(_collectedSoftOnlevel);
        //_settings.SoftCurrency += _collectedSoftOnlevel;
        collectedSoftOnLevel = 0;
    }

    public void AddSoft(int amount)
    {
        _settings.SoftCurrency += amount;
        OnSoftChanged?.Invoke(_settings.SoftCurrency);
    }

    public bool SubstructSoft(int amount)
    {
        if (_settings.SoftCurrency - amount < 0)
            return false;
        _settings.SoftCurrency -= amount;
        OnSoftChanged?.Invoke(_settings.SoftCurrency);
        return true;
    }

    public void CallBackRequest()
    {
        OnSoftChanged?.Invoke(_settings.SoftCurrency);
    }

    [Serializable]
    public class Settings
    {
        public int SoftCurrency;
    }
}
