﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider)),RequireComponent(typeof(Rigidbody))]
public class Picker : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var pickableItem = other.gameObject.GetComponent<IPickable>();

        if ( pickableItem != null)
        {
            Debug.Log(pickableItem);
            pickableItem.Pick();
        }
    }
}
