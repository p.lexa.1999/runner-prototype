﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Move : MonoBehaviour
{
    private Settings _settings;
    private InputHandler _input;
    private VirtualBorder _border;
    private GroundChecker _groundChecker;
    private RailsChecker _railsChecker;
    private ArcMoving _arcMoving;


    private float _mobileDeviceMultiplyer;
    private float _speedMultiplyerOnRails;
    private float _speedMultiplyerOnRoll;

    [Inject]
    public void Constructor(Settings settings,InputHandler input,VirtualBorder border,GroundChecker groundChecker,RailsChecker railsChecker,ArcMoving arcMoving)
    {

        _settings = settings;
        _input = input;
        _border = border;
        _railsChecker = railsChecker;
        _groundChecker = groundChecker;
        _arcMoving = arcMoving;

        _speedMultiplyerOnRails = 1;
        _speedMultiplyerOnRoll = 1;
        _mobileDeviceMultiplyer = _settings.mobileDeviceMultiplyer;

        #if UNITY_EDITOR
        _mobileDeviceMultiplyer = 1;
        #endif
        
    }

    void FixedUpdate()
    {
        SpeedMultiplyerSet();
        MoveForward();
        MoveSides();
    }

    private void MoveForward()
    {
        transform.position +=Vector3.forward * _settings.movingForwardSpeed * _speedMultiplyerOnRails  * _speedMultiplyerOnRoll * Time.deltaTime;
    }

    private void MoveSides()
    {
        var futurePos = transform.position + Vector3.right * _input.DeltaMove.x * _settings.slidingSpeed * 
                        _mobileDeviceMultiplyer * Time.deltaTime;
        transform.position = _border.Clamp(futurePos);        
    }

    private void SpeedMultiplyerSet()
    {
        if (_groundChecker.IsOnGround)
        {
            _speedMultiplyerOnRails = 1;
        }
        if(_railsChecker.IsOnRails && !_groundChecker.IsOnGround)
        {
            _speedMultiplyerOnRails = _settings.speedMultiplyerOnRails;
        }
        if (_arcMoving.isActive)
        {
            _speedMultiplyerOnRoll = _settings.speedMultiplyerOnRoll;
        }
        else
        {
            _speedMultiplyerOnRoll = 1;
        }
    }

    [Serializable]
    public class Settings
    {
        public float movingForwardSpeed;
        public float slidingSpeed;
        public float speedMultiplyerOnRails;
        public float speedMultiplyerOnRoll;
        public float mobileDeviceMultiplyer;

    }
}
