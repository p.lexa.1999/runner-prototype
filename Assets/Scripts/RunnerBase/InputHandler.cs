﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InputHandler :IFixedTickable
{
    public Vector3 DeltaMove
    {
        get
        {
            // CountDeltaMove();
            // var res = _deltaMove;
            // _deltaMove = Vector3.zero;
            // return res;
            return _deltaMove;
        }
        set { }
    }
    private Vector3 _deltaMove;
    private Vector3 _previousMousePos;

    public InputHandler()
    {

    }

    public void FixedTick()
    {
        CountDeltaMove();
    }

    public void CountDeltaMove()
    {
#if UNITY_EDITOR
        MouseInput();
#endif

#if UNITY_IOS
            MobileInput();
#endif

#if UNITY_ANDROID
            MobileInput();
#endif
    }

    private void MobileInput()
    {
        if (Input.touchCount > 0)
            _deltaMove = Input.GetTouch(0).deltaPosition;
        else
            _deltaMove = Vector3.zero;
    }

    private void MouseInput()
    {
        var mousePos = Input.mousePosition;
        _deltaMove = mousePos - _previousMousePos;
        _previousMousePos = mousePos;
    }
}
