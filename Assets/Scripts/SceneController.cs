﻿using Zenject;
using UnityEngine.SceneManagement;

public class SceneController
{
    [Inject]
    public SceneController()
    {

    }

    public void ReloadScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
