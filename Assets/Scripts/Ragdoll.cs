﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    private Animator _anim;
    private AnimationController _animController;

    private Rigidbody[] _bodyParts;


    private void Awake()
    {
        //col = GetComponent<CapsuleCollider>();
        _anim = GetComponent<Animator>();
        _animController = GetComponent<AnimationController>();
        _bodyParts = GetComponentsInChildren<Rigidbody>();
        TurnAnimation();
        //TurnRagdoll();
    }

    public void TurnRagdoll()
    {
        transform.parent = null;
        Logic(true);
    }


    public void TurnAnimation()
    {
        Logic(false);
    }


    private void Logic(bool isRag)
    {
        foreach (Rigidbody r in _bodyParts)
        {
            r.isKinematic = !isRag;

        }

        //if(col != null)
        //    col.enabled = !isRag;
        _anim.enabled = !isRag;
        _animController.enabled = !isRag;

        
    }
    
}
