﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PickableStick : MonoBehaviour, IPickable
{
    [Inject]
    private IStickable _stick;

    

    public void Pick()
    {
        _stick.Extend(Mathf.RoundToInt(transform.localScale.y*2f*5));
        Destroy(gameObject);
    }
}
