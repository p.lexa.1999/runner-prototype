﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GameSettings")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{
    public PlayerSettings Player;
    public TrackSettings Track;
    public LavaFloor.Settings LavaFloor;
    public LevelManager.Settings Level;
    public Currency.Settings Currency;

    public override void InstallBindings()
    {
        Container.BindInstance(Player.Moving);
        Container.BindInstance(Player.Falling);
        Container.BindInstance(Player.Animations);
        Container.BindInstance(Track.VirtualBorders);
        Container.BindInstance(LavaFloor);
        Container.BindInstances(Level);
        Container.BindInstances(Player.Rolling);
        Container.BindInstances(Currency);
    }

    [Serializable]
    public class PlayerSettings
    {
        public Move.Settings Moving;
        public Fly.Settings Falling;
        public ArcMoving.Settings Rolling;
        public AnimationController.Settings Animations;
    }

    [Serializable]
    public class TrackSettings
    {
        public VirtualBorder.Settings VirtualBorders;
    }
}
