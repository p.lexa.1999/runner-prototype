using System;
using UnityEngine;
using Zenject;


public class Installer : MonoInstaller
{
    //[Inject]
    //Settings _settings = null;
    [SerializeField] GameObject player;
    [SerializeField] GameObject canvas;

    public override void InstallBindings()
    {
        Container.Bind<Installer>().FromInstance(this);
        Container.Bind<LevelManager>().AsSingle().NonLazy();
        Container.Bind<VirtualBorder>().AsSingle();
        Container.Bind<LoseManager>().AsSingle();
        Container.Bind<WinManager>().AsSingle();
        Container.Bind<SceneController>().AsSingle();
        Container.Bind<CutArcDestroyer>().AsSingle();
        Container.Bind<Currency>().AsSingle();
        Container.BindInterfacesAndSelfTo<InputHandler>().AsSingle().NonLazy();

        Container.Bind<AnimationController>().FromInstance(player.GetComponentInChildren<AnimationController>());
        Container.Bind<IStickable>().FromInstance(player.GetComponentInChildren<IStickable>()).Lazy().IfNotBound();
        Container.Bind<Ragdoll>().FromInstance(player.GetComponentInChildren<Ragdoll>());
        Container.Bind<GroundChecker>().FromInstance(player.GetComponentInChildren<GroundChecker>());
        Container.Bind<RailsChecker>().FromInstance(player.GetComponentInChildren<RailsChecker>());
        Container.Bind<ArcMoving>().FromInstance(player.GetComponentInChildren<ArcMoving>());
        Container.Bind<Move>().FromInstance(player.GetComponentInChildren<Move>());
        Container.Bind<ArcManager>().FromInstance(player.GetComponentInChildren<ArcManager>());

        Container.Bind<CameraFollow>().FromInstance(Camera.main.GetComponent<CameraFollow>());

        Container.Bind<UiRestartPanel>().FromInstance(canvas.GetComponent<UiRestartPanel>());
        Container.Bind<UiWinPanel>().FromInstance(canvas.GetComponent<UiWinPanel>());
        Container.Bind<UIPick>().FromInstance(canvas.GetComponentInChildren<UIPick>());

    }

    public bool LoadLevel(int levelNum)
    {
        GameObject level = Resources.Load<GameObject>("Levels/Level" + levelNum);
        
        //GameObject level = Container.InstantiatePrefabResource("Levels/Level"+levelNum);
        Debug.Log(level);
        if (level == null)
        {
            return false;
        }
        level = Container.InstantiatePrefab(level);
        if(level.GetComponent<Level>() == null)
            level.AddComponent<Level>();
        canvas.GetComponent<UiGamePanel>().DisplayLevelNumber(levelNum);
        return true;
    }

    // [Serializable]
    // public class Settings
    // {
    //public GameObject ExplosionPrefab;
    // }
}