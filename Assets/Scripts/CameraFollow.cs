﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] GameObject followObject;
    [SerializeField] Vector3 followOffset;

    private bool _isStopped = false;

    void FixedUpdate()
    {
        if (!_isStopped)
        {
            var pos = followObject.transform.position + followOffset;
            transform.position = new Vector3(transform.position.x, pos.y, pos.z);
        }
    }

    public void Stop()
    {
        _isStopped = true;
    }
}
