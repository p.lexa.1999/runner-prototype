﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

public class AnimationController : MonoBehaviour
{
    public event Action OnJumpEnd;

    private Settings _settings;
    private IStickable _stick;
    private Animator _animator;
    private Animator _parentAnimator;


    [Inject]
    public void Constructor(Settings settings,IStickable stick)
    {
        _settings = settings;
        _stick = stick;
    }

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _parentAnimator = transform.parent.GetComponent<Animator>();
    }

    public void StartRunning()
    {
        _animator.SetBool("IsStarted", true);
    }

    public void Fly()
    {
        _animator.SetBool("IsGrounded",false);
        _stick.SmoothStickMove(_settings.anotherStickPos,false);
    }

    public void Run()
    {
        _animator.SetBool("IsGrounded", true);
        _stick.SmoothStickMove(_settings.runStickPos,false);
    }

    public void GoRails()
    {
        _animator.SetBool("IsOnRails", true);
    }

    public void LeaveRails()
    {
        _animator.SetBool("IsOnRails", false);

    }

    public void Jump(float heigh)
    {
         _animator.SetBool("IsJump", true);
         _parentAnimator.SetBool("IsJump",true);
        //StartCoroutine(TakePose(heigh));
    }
    
    public void EndJump()
    {
        OnJumpEnd?.Invoke();
        Debug.Log("END");
        _animator.enabled = false;
        _parentAnimator.enabled = false;
        //transform.parent.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
    }

    private IEnumerator TakePose(float heigh)
    {
        var parent = transform.parent;
        var startRot = parent.rotation.eulerAngles;
        var endRot = new Vector3(0, 0, 90);
        float max = 1 + 0.1f;
        float period = 0.01f;

        var starPos = parent.position;
        var endPos = parent.position + Vector3.up;

        for (float i = 0; i <= max; i += period)
        {
            //parent.position += Vector3.up * 0.05f;
            parent.position = Vector3.Lerp(parent.position, parent.position + Vector3.up * 0.001f, i);
            yield return new WaitForSeconds(0.01f);
        }

        max = 1 + 0.1f;
        period = 0.03f;

        for (float i = 0; i <= max; i += period)
        {
            parent.rotation = Quaternion.Euler(Vector3.Lerp(startRot, endRot, i));
            //parent.position += Vector3.up * heigh /max*period;//(floorPos.y - arc.transform.position.y + arc.transform.GetChild(0).localScale.x * 2f);
            //parent.position -= Vector3.up * (heigh) / max/period;
            yield return new WaitForSeconds(0.03f);
        }


        EndJump();
    }

    

    [Serializable]
    public class Settings
    {
        public Vector3 runStickPos;
        public Vector3 anotherStickPos;

    }
}
