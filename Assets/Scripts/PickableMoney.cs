﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PickableMoney : MonoBehaviour, IPickable
{
    [Inject]
    private Currency _currency;

    public void Pick()
    {
        _currency.CollectSoft(1);
        Destroy(gameObject);
    }
}
