﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FinishTrigger : MonoBehaviour
{
    private ArcMoving player;

    private void Start()
    {
        player = FindObjectOfType<ArcMoving>();
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player.gameObject)
        {
            player.RotateAndMove();
        }
    }

    
}
