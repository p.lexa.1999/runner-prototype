﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IStickable
{
    event Action OnStickEnd;
    void Extend(int value);
    void Shrink(float value);
    void BecomeFree(Transform objectToParent);
    void SmoothStickMove(Vector3 distinationPlace, bool isStartDelay);
}
