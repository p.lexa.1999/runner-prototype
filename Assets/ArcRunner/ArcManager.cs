﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ArcManager : MonoBehaviour, IStickable
{

    public event Action<int> OnExtend;
    public event Action OnStickEnd;
    
    [SerializeField]
    private ArcBeh arcPrefab;
    private List<ArcBeh> arcs;

    [SerializeField]
    private Transform arcsParent;

    [SerializeField]
    private float alpha, initRadius;

    [SerializeField]
    private float radiusIncr0;
    private float arcLen;
    private float angleSum;

    private ArcColliderManager _colliderManager;
    
    private CutArcDestroyer _cutArcDestroyer;
   
    [Inject]
    public void Constructor(CutArcDestroyer cutArcDestroyer)
    {
        _cutArcDestroyer = cutArcDestroyer;
    }


    void Start()
    {
        _colliderManager = GetComponent<ArcColliderManager>();

        arcLen = Mathf.Tan(alpha * 0.5f * Mathf.Deg2Rad) * initRadius * 2;
        arcs = new List<ArcBeh>();
        angleSum = 0;
    }

    public void Extend(int value)
    {
        OnExtend?.Invoke(value);

        for (int i = 0; i < value; i++)
        {
            AddArc();
        }
    }

    public void Shrink(float value)
    {
        for (int i = 0; i < value; i++)
        {
            if (arcs.Count == 0)
            {
                OnStickEnd?.Invoke();
                return;
            }
            RemoveArc(arcs[arcs.Count - 1],false);
        }
    }

    public void Cut(ArcBeh arc)
    {
        RemoveArc(arc,true);
    }

    public void AddArc()
    {
        var arc = Instantiate(arcPrefab, arcsParent);
        arc.transform.localPosition = Vector3.zero;
        if (arcs.Count > 0) {
            arc.transform.localEulerAngles = arcs[arcs.Count - 1].transform.localEulerAngles;
        } else {
            arc.transform.localEulerAngles = Vector3.zero;
        }
        float radAddition = radiusIncr0 * (angleSum / 360f);// * (1 + Mathf.Abs(Vector3.Dot(Vector3.right, arc.transform.right)));
        float angleAddition = -Mathf.Atan2(arcLen, initRadius + radAddition) * Mathf.Rad2Deg;
        arc.transform.localEulerAngles += Vector3.up * angleAddition;
        arc.transform.localPosition += arc.transform.right * radAddition;
        arcs.Add(arc);
        CountColliderLength();

        arc.Init(
            angleAddition: -angleAddition
        );

        angleSum += -angleAddition;
    }

    public void RemoveArc(ArcBeh arc,bool isCut)
    {

        var index = arcs.IndexOf(arc);
        Debug.Log(index);

        List<GameObject> destroyList = new List<GameObject>();

        for(int i = arcs.Count - 1; i >= index; i--)
        {
            angleSum -= arcs[i].AngleAddition;
            var child = arcs[i].transform.GetChild(0);

            child.transform.parent = null;

            if (isCut)
            {
                destroyList.Add(child.gameObject);
            }
            else
            {
                child.GetComponent<CapsuleCollider>().isTrigger = false;
                child.GetComponent<Rigidbody>().isKinematic = false;
                child.gameObject.AddComponent<DestroyAfterTime>().time = 5f;
                Destroy(arcs[i].gameObject);
            }
            
            arcs.Remove(arcs[i]);
        }

        if (isCut)
        {
            StartCoroutine(_cutArcDestroyer.SmoothDestroy(destroyList));
        }

        CountColliderLength();
    }
    
    public void Roll()
    {
        var index = arcs.Count - 1;
        if(index < 0)
        {
            Debug.Log("Finish");
            return;
        }

        angleSum -= arcs[index].AngleAddition;
        arcs[index].transform.parent = null;
        arcs.Remove(arcs[index]);
        
    }
    
    private void CountColliderLength()
    {
        if(arcs.Count!=0)
            _colliderManager.CountColliderLength(arcs[arcs.Count-1].transform.GetChild(0).position);
    } 

    public void SmoothStickMove(Vector3 distinationPlace, bool isStartDelay)
    {
        //interface realiation
    }

    public ArcBeh GetLastArc()
    {
        if (arcs.Count - 1 < 0)
            return null;

        return arcs[arcs.Count - 1];
    }

    public void BecomeFree(Transform objToParent)
    {
        Debug.Log("Free");
        transform.parent = objToParent;
        _colliderManager.PrepareCollidersForDeath();
        gameObject.AddComponent<Rigidbody>();//.AddForce(new Vector3(0,5000,50),ForceMode.Force);
    }

    public void CrushArc()
    {
        foreach(ArcBeh arc in arcs)
        {
            arc.GetComponentInChildren<CapsuleCollider>().isTrigger = false;
            arc.GetComponentInChildren<Rigidbody>().isKinematic = false;
            arc.transform.parent = null;
            Destroy(arc.gameObject);
        }
    }

    public void PickableArcs()
    {
        foreach (ArcBeh arc in arcs)
        {
            arc.gameObject.AddComponent<Picker>();
        }
    }
}
