﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcColliderManager : MonoBehaviour
{
    private BoxCollider _centralCollider;
    private BoxCollider _frontCollider;
    private BoxCollider _backCollider;

    private Vector3 _frontColSize;
    private Vector3 _frontColPos;

    private float magicCoef = 1.3f;

    private void Start()
    {
        var cols = GetComponents<BoxCollider>();
        _frontCollider = cols[0];
        _centralCollider = cols[1];
        _backCollider = cols[2];

        _frontCollider.center = Vector3.zero;
        _centralCollider.center = Vector3.zero;
        _backCollider.center = Vector3.zero;

        _frontCollider.size = Vector3.zero;
    }

    public void CountColliderLength(Vector3 pos)
    {
        var dist = Vector3.Distance(transform.position, pos);
        
        _centralCollider.size = new Vector3(dist * 2f,0.1f,dist/2f);
        _frontColSize = new Vector3(dist*2f / magicCoef,0.1f, dist / 2f);
        _backCollider.size = new Vector3(dist * 2f /magicCoef, 0.1f, dist / 2f);

        _backCollider.center = new Vector3(0,0, -dist / 2f);
        _frontColPos = new Vector3(0, 0, dist / 2f);
        //lenght back and forward coefs 1.3
    }
    
    public void PrepareCollidersForDeath()
    {
        _frontCollider.enabled = true;
        _backCollider.enabled = true;
        _centralCollider.enabled = true;

        _frontCollider.center = _frontColPos;
        _frontCollider.size = _frontColSize;
        _frontCollider.isTrigger = false;
        _backCollider.isTrigger = false;
        _centralCollider.isTrigger = false;
    }
}
