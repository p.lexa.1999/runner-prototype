﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ArcMoving : MonoBehaviour
{
    public bool isActive { get; private set; }
    private ArcManager _arcManager;
    private float _timer = 0;
    private float _timeToRoll = 0.001f;
    private Vector3 floorPos;
    private Vector3 deltaRotation;

    private float _mobileDeviceMultiplyer;

    [SerializeField] GameObject charObj;
    [SerializeField] GameObject arcObj;

    private WinManager _win;
    private InputHandler _inputHandler;
    private AnimationController _animation;
    private Settings _settings;

    [Inject]
    public void Constructor(WinManager win,InputHandler input,AnimationController animation,Settings settings)
    {
        _win = win;
        _inputHandler = input;
        _animation = animation;
        _settings = settings;
        _mobileDeviceMultiplyer = _settings.rollMobileDeviceMultiplyer;

#if UNITY_EDITOR
        _mobileDeviceMultiplyer = 1;
#endif
    }

    void Start()
    {
        _arcManager = GetComponentInChildren<ArcManager>();
        isActive = false;
    }

    void FixedUpdate()
    {
        if (isActive)
        {
            var angle = transform.rotation.eulerAngles.y;
            if (angle > 180)
            {
                angle -= 360;
            }
            transform.Rotate(Vector3.down * angle/10f);
            transform.Rotate(Vector3.up * _inputHandler.DeltaMove.x * 2f * Time.deltaTime * _mobileDeviceMultiplyer);
            
        }

        if (isActive && _timer >= _timeToRoll)
        {
            _timer = 0;
            var arc = _arcManager.GetLastArc();
            if (arc == null)
            {
                isActive = false;
                _win.Win();
                return;
            }
            
            charObj.transform.Rotate(Vector3.down * arc.transform.rotation.eulerAngles.x);
            arcObj.transform.Rotate(Vector3.down * arc.transform.rotation.eulerAngles.x);
            transform.position += Vector3.up*(floorPos.y - arc.transform.position.y+ arc.transform.GetChild(0).localScale.x*2f);
            _arcManager.Roll();
        }

        _timer += Time.deltaTime;
    }

    public void RotateAndMove()
    {
        //StartCoroutine(TakePose());
        //anim

        var arc = _arcManager.GetLastArc();
        if (arc == null)
        {
            _win.Win();
            return;
        }

        GetComponent<Fly>().enabled = false;
        floorPos = transform.position;
        _animation.Jump(floorPos.y - arc.transform.position.y + arc.transform.GetChild(0).localScale.x * 2f);
        _animation.OnJumpEnd += StartRolling;
    }



    public void StartRolling()
    {
        isActive = true;
    }

    [Serializable]
    public class Settings
    {
        public float rollMobileDeviceMultiplyer;
    }

}
