﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcCollisionDetector : MonoBehaviour
{
    private RailsChecker _checker;

    private void Start()
    {
        _checker = GetComponentInParent<RailsChecker>();
    }

    private void OnTriggerEnter(Collider other)
    {
        _checker.Enter(other);   
    }

    private void OnTriggerExit(Collider other)
    {
        _checker.Exit(other);
    }
}
