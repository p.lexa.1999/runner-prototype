﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutArcDestroyer
{

    public IEnumerator SmoothDestroy(List<GameObject> list)
    {
        for (int i = list.Count - 1; i >= 0; i--)
        {
            var child = list[i];
            
            child.GetComponent<CapsuleCollider>().isTrigger = false;
            child.GetComponent<Rigidbody>().isKinematic = false;
            child.gameObject.AddComponent<DestroyAfterTime>().time = 5f;
            if( i%2 == 0)
                yield return new WaitForEndOfFrame();
        }
    }

}
