﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcBeh : MonoBehaviour {
    public float AngleAddition {get; private set;}


    public void Init(float angleAddition) {
        this.AngleAddition = angleAddition;
    }
}
